<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\URL;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ResidentsSearchModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Residents Models';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="residents-model-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'label' => 'Имя',
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a(Html::encode($data->name), Url::toRoute(['residents/view', 'id' => $data->id]));
                },
            ],
            'gender',
            'homeworld',
        ],
    ]);
    ?>
</div>
