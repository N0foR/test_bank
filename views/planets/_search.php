<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PlanetsSearchModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="planets-model-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'rotation_period') ?>

    <?= $form->field($model, 'orbital_period') ?>

    <?= $form->field($model, 'diameter') ?>

    <?php // echo $form->field($model, 'climate') ?>

    <?php // echo $form->field($model, 'gravity') ?>

    <?php // echo $form->field($model, 'terrain') ?>

    <?php // echo $form->field($model, 'surface_water') ?>

    <?php // echo $form->field($model, 'population') ?>

    <?php // echo $form->field($model, 'url') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
