<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\URL;

/* @var $this yii\web\View */
/* @var $model app\models\PlanetsModel */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Planets Models', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="planets-model-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'rotation_period',
            'orbital_period',
            'diameter',
            'climate',
            'gravity',
            'terrain',
            'surface_water',
            'population',
        ],
    ])
    ?>

    <? if (!empty($model->residents)): ?>
        <h2><?= Html::encode('Персонажи на планете') ?></h1>
            <table>
                <tbody>

                    <? foreach ($model->residents as $item): ?>
                        <tr>
                            <td>

                                <?= Html::a(Html::encode($item->name), Url::toRoute(['residents/view', 'id' => $item->id])); ?>
                            </td>
                        </tr>
                    <? endforeach; ?>
                </tbody>
            </table>           
        <? endif; ?>

</div>
