<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\URL;

$this->title = 'Отчет';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Отчет о планетах  <?= empty($count) ? " без персонажей" : "содержащих персонажей в кол-ве {$count}" ?></h1>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-6">
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'label' => 'Планета',
                            'format' => 'raw',
                            'value' => function ($data) {
                                //  return Html::a(Html::encode($data->planets->name), Url::toRoute(['planets/view', 'id' => $data->planets->id]));
                                return Html::a(Html::encode($data->planetsBySqlRelation->name), Url::toRoute(['planets/view', 'id' => $data->planetsBySqlRelation->id]));
                            },
                        ],
                    ],
                ]);
                ?>
            </div>


        </div>

    </div>
</div>
