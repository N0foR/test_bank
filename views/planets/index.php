<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\URL;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PlanetsSearchModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Planets Models';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="planets-model-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'label' => 'Название',
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a(Html::encode($data->name), Url::toRoute(['planets/view', 'id' => $data->id]));
                },
            ],
            'rotation_period',
            'orbital_period',
            'diameter',
        //'climate',
        //'gravity',
        //'terrain',
        //'surface_water',
        //'population',
        ],
    ]);
    ?>
</div>
