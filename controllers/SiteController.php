<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use app\models\PlanetsResidentsModel;

class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],

        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {

        $model = new PlanetsResidentsModel();
        $dataProvider=$model->getCountPeopleOnPlanet();
       // var_dump(PlanetsResidentsModel::getCountPeopleOnPlanet());
        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    //@ToDo curl -i -u token: "http://test.n0for.ru/api/weather/weather"
}
