<?php

namespace app\controllers;

use Yii;
use app\models\PlanetsModel;
use app\models\PlanetsSearchModel;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PlanetsController implements the CRUD actions for PlanetsModel model.
 */
class PlanetsController extends Controller {

    public $defaultAction = 'index';

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PlanetsModel models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new PlanetsSearchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PlanetsModel model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the PlanetsModel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PlanetsModel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = PlanetsModel::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionReport($count = 0) {
        $dataProvider = \app\models\PlanetsResidentsModel::getCountPeopleOnPlanet($count);
        return $this->render('report', [
                    'dataProvider' => $dataProvider,
                    'count' => $count,
        ]);
    }


}
