<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use app\models\ResidentsModel;
use app\models\PlanetsResidentsModel;

/**
 * Контроллер отвечает за команду yii get-residents
 * Позвоялет загружать информацию о резидентах
 */
class GetResidentsController extends Controller {

    /**
     * Загружает информацию о резидентах
     */
    public function actionIndex() {
        $residents = \Yii::$app->swapig->getResidents();
        $count_residents_update = $count_residents = $count_residents_insert = array();
        foreach ($residents as $item) {
            if (($modelResident = ResidentsModel::findByUrl($item['url'])) === null) {
                $model = new ResidentsModel();
                $model->name = $item['name'];
                $model->gender = $item['gender'];
                $model->homeworld = $item['homeworld'];
                $model->url = $item['url'];
                $model->isNewRecord = true;
                $count_residents[] = $model->save();
                $id = $model->id;
            }

            if (($modelPlanetsResident = PlanetsResidentsModel::findByResidentUrl($item['url'])) === null) {
                $model = new PlanetsResidentsModel();
                if (isset($modelResident)) {
                    $model->id_resident = $modelResident->id;
                } else {
                    $model->id_resident = $id;
                }
                $model->resident_url = $item['url'];
                $model->isNewRecord = true;
                $count_residents_insert[] = $model->save();
            } else {
                if (isset($modelResident)) {
                    if ($modelPlanetsResident->id_resident != $modelResident->id) {
                        $modelPlanetsResident->id_resident = $modelResident->id;
                        $count_residents_update[] = $modelPlanetsResident->save();
                    }
                } else {
                    $modelPlanetsResident->id_resident = $id;
                    $count_residents_update[] = $modelPlanetsResident->save();
                }
            }
        }
        $str = '';
        if (!empty($count_planets)) {
            $str = "Insert residents: " . count($count_planets) . "\n";
        }
        if (!empty($count_residents_insert)) {
            $str .= "Insert residents on planet: " . count($count_residents_insert) . "\n";
        }
        if (!empty($count_residents_update)) {
            $str .= "Update residents on place: " . count($count_residents_update) . "\n";
        }
        echo $str;
        return ExitCode::OK;
    }

}
