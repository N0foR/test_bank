<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use app\models\PlanetsModel;
use app\models\PlanetsResidentsModel;

/**
 * Контроллер отвечает за команду yii get-planets
 * Позвоялет загружать информацию о пдантеах
 */
class GetPlanetsController extends Controller {

    /**
     * Загружает информацию о планетах
     */
    public function actionIndex() {
        $planets = \Yii::$app->swapig->getPlanets();
        $count_residents_update = $count_planets = $count_residents_insert = array();
        foreach ($planets as $item) {
            if (($modelPlanet = PlanetsModel::findByUrl($item['url'])) === null) {
                $model = new PlanetsModel();
                $model->name = $item['name'];
                $model->rotation_period = $item['rotation_period'];
                $model->orbital_period = $item['orbital_period'];
                $model->diameter = $item['diameter'];
                $model->climate = $item['climate'];
                $model->gravity = $item['gravity'];
                $model->terrain = $item['terrain'];
                $model->surface_water = $item['surface_water'];
                $model->population = $item['population'];
                $model->url = $item['url'];
                $model->isNewRecord = true;
                $count_planets[] = $model->save();
                $id = $model->id;
            }
            if (!empty($item['residents'])) {
                foreach ($item['residents'] as $resident) {
                    if (($modelPlanetsResident = PlanetsResidentsModel::findByResidentUrl($resident)) === null) {
                        $model = new PlanetsResidentsModel();
                        if (isset($modelPlanet)) {
                            $model->id_planets = $modelPlanet->id;
                        } else {
                            $model->id_planets = $id;
                        }
                        $model->resident_url = $resident;
                        $model->isNewRecord = true;
                        $count_residents_insert[] = $model->save();
                    } else {
                        if (isset($modelPlanet)) {
                            if ($modelPlanetsResident->id_planets != $modelPlanet->id) {
                                $modelPlanetsResident->id_planets = $modelPlanet->id;
                                $count_residents_update[] = $modelPlanetsResident->save();
                            }
                        } else {
                            $modelPlanetsResident->id_planets = $id;
                            $count_residents_update[] = $modelPlanetsResident->save();
                        }
                    }
                }
            }
        }
        $str = '';
      
        if (!empty($count_planets)) {
            $str = "Insert planet: " . count($count_planets) . "\n";
        }
        if (!empty($count_residents_insert)) {
            $str .= "Insert residents om planet: " . count($count_residents_insert) . "\n";
        }
        if (!empty($count_residents_update)) {
            $str .= "Update residents on place: " . count($count_residents_update) . "\n";
        }
        echo $str;
        return ExitCode::OK;
    }

}
