<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "residents_in_planet".
 *
 * @property int $id
 * @property int $id_planets
 * @property int $id_resident
 * @property string $resident_url
 *
 * @property Planets $planets
 * @property Residents $resident
 */
class PlanetsResidentsModel extends \yii\db\ActiveRecord {

    public $planet;
    public $count_of_people;

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'residents_in_planet';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['resident_url'], 'required'],
            [['id', 'id_planets', 'id_resident'], 'integer'],
            [['resident_url'], 'string', 'max' => 200],
            [['id_planets'], 'exist', 'skipOnError' => true, 'targetClass' => PlanetsModel::className(), 'targetAttribute' => ['id_planets' => 'id']],
            [['id_resident'], 'exist', 'skipOnError' => true, 'targetClass' => ResidentsModel::className(), 'targetAttribute' => ['id_resident' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'id_planets' => 'Id Planets',
            'id_resident' => 'Id Resident',
            'resident_url' => 'Resident Url',
            'count_of_people' => 'Кол-во персонажей',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlanets() {
        return $this->hasOne(PlanetsModel::className(), ['id' => 'id_planets']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResident() {
        return $this->hasOne(ResidentsModel::className(), ['id' => 'id_resident']);
    }

    public function findByResidentUrl($url) {
        $model = PlanetsResidentsModel::find()->where(['resident_url' => $url])->one();
        if ($model === null) {
            return null;
        } else {
            return $model;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlanetsBySqlRelation() {
        return $this->hasOne(PlanetsModel::className(), ['id' => 'planet']);
    }

    public function getCountPeopleOnPlanet(int $count = -1) {

        if ($count >= 0) {
            $query = PlanetsResidentsModel::find()->select('planets.id as planet, COUNT(id_resident) as count_of_people')->rightJoin('planets', '`planets`.`id` = `id_planets`')->with('planetsBySqlRelation')->groupBy(['planet'])->having(['COUNT(id_resident)' => $count])->orderby('count_of_people DESC, planet DESC');
        } else {
            $query = PlanetsResidentsModel::find()->select('planets.id as planet, COUNT(id_resident) as count_of_people')->rightJoin('planets', '`planets`.`id` = `id_planets`')->with('planetsBySqlRelation')->groupBy(['planet'])->orderby('count_of_people DESC, planet DESC');
        }

        /*  $sql = 'SELECT t.id as planet, id_planets, COUNT(id_resident) as count_of_people '
          . 'FROM planets as t '
          . 'LEFT JOIN residents_in_planet ON residents_in_planet.id_planets=t.id '
          . 'GROUP BY planet '
          . 'ORDER BY count_of_people DESC, planet '
          . 'LIMIT 30';
          if ($count) {
          $query = PlanetsResidentsModel::findBySql($sql, [])->with('planetsBySqlRelation')->having(['COUNT(id_resident)' => $count]);
          } else {
          $query = PlanetsResidentsModel::findBySql($sql, [])->with('planetsBySqlRelation');
          } */

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        return $dataProvider;
    }

}
