<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "residents".
 *
 * @property int $id
 * @property string $name Название
 * @property string $gender Пол
 * @property string $homeworld Родной мир
 * @property string $url
 *
 * @property ResidentsInPlanet[] $residentsInPlanets
 */
class ResidentsModel extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'residents';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['name', 'gender', 'homeworld', 'url'], 'required'],
            [['name', 'gender', 'homeworld', 'url'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'gender' => 'Пол',
            'homeworld' => 'Родной мир',
            'url' => 'Url',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResidentsInPlanets() {
        return $this->hasMany(ResidentsInPlanet::className(), ['id_resident' => 'id']);
    }

    public function findByUrl($url) {
        $model = ResidentsModel::find()->where(['url' => $url])->one();
        if ($model === null) {
            return null;
        } else {
            return $model;
        }
    }

}
