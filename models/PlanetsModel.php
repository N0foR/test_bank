<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "planets".
 *
 * @property int $id
 * @property string $name Название
 * @property int $rotation_period Период вращения по своей оси
 * @property int $orbital_period Период обращения по орбите звезды
 * @property int $diameter Диаметр
 * @property string $climate Климат
 * @property string $gravity Гравитация
 * @property string $terrain Особенности
 * @property int $surface_water Покрытие водой
 * @property int $population Кол-во жителей
 * @property string $url
 *
 * @property ResidentsInPlanet[] $residentsInPlanets
 */
class PlanetsModel extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'planets';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['name', 'rotation_period', 'orbital_period', 'diameter', 'climate', 'gravity', 'terrain', 'surface_water', 'population', 'url'], 'required'],
            [['rotation_period', 'orbital_period', 'diameter', 'surface_water', 'name', 'climate', 'gravity', 'terrain', 'url', 'population'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'rotation_period' => 'Период вращения по своей оси',
            'orbital_period' => 'Период обращения по орбите звезды',
            'diameter' => 'Диаметр',
            'climate' => 'Климат',
            'gravity' => 'Гравитация',
            'terrain' => 'Особенности',
            'surface_water' => 'Покрытие водой',
            'population' => 'Кол-во жителей',
            'url' => 'Url',
            'count_of_people' => 'Кол-во персонажей н апланете',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResidentsInPlanets() {
        return $this->hasMany(ResidentsInPlanet::className(), ['id_planets' => 'id']);
    }

    public function findByUrl($url) {
        $model = PlanetsModel::find()->where(['url' => $url])->one();
        if ($model === null) {
            return null;
        } else {
            return $model;
        }
    }

    /**
     *  Функция преобразует найденные данные в необходимый вид
     */
    public function afterFind() {

        if (!empty($this->surface_water) && is_numeric($this->surface_water)) {
            $this->surface_water .= '%';
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlanetResidents() {
        return $this->hasMany(PlanetsResidentsModel::className(), ['id_planets' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResidents() {
        return $this->hasMany(ResidentsModel::className(), ['id' => 'id_resident'])->via('planetResidents');
    }

}
