<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PlanetsModel;

/**
 * PlanetsSearchModel represents the model behind the search form of `app\models\PlanetsModel`.
 */
class PlanetsSearchModel extends PlanetsModel {

    public $count_of_people = 0;

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id', 'rotation_period', 'orbital_period', 'diameter', 'surface_water', 'population'], 'integer'],
            [['name', 'climate', 'gravity', 'terrain', 'url'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = PlanetsModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'rotation_period' => $this->rotation_period,
            'orbital_period' => $this->orbital_period,
            'diameter' => $this->diameter,
            'surface_water' => $this->surface_water,
            'population' => $this->population,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
                ->andFilterWhere(['like', 'climate', $this->climate])
                ->andFilterWhere(['like', 'gravity', $this->gravity])
                ->andFilterWhere(['like', 'terrain', $this->terrain])
                ->andFilterWhere(['like', 'url', $this->url]);

        return $dataProvider;
    }

}
