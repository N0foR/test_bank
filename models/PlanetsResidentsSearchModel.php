<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PlanetsResidentsModel;

/**
 * PlanetsResidentsSearchModel represents the model behind the search form of `app\models\PlanetsResidentsModel`.
 */
class PlanetsResidentsSearchModel extends PlanetsResidentsModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_planets', 'id_resident'], 'integer'],
            [['resident_url'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PlanetsResidentsModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_planets' => $this->id_planets,
            'id_resident' => $this->id_resident,
        ]);

        $query->andFilterWhere(['like', 'resident_url', $this->resident_url]);

        return $dataProvider;
    }
}
