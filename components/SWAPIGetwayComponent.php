<?php

namespace app\components;

use yii\base\Component;
use SWAPI\SWAPI;
use JsonMapper;

class SWAPIGetwayComponent extends Component { // объявляем класс

    private $swapi;
    private $residents = array();
    private $palnets = array();

    public function init() {
        parent::init();
        $this->swapi = new SWAPI;
        //необ
        $newJsonMapper = new JsonMapper();
        $newJsonMapper->bEnforceMapType = false;
        $this->swapi->setMapper($newJsonMapper);
    }

    public function getResidents() {
        if (!empty($this->residents)) { //проверка строки на пустоту
            return $this->residents;
        }
        do {
            if (!isset($residents)) {
                $residents = $this->swapi->characters()->index();
            } else {
                $residents = $residents->getNext();
            }

            foreach ($residents as $resident) {
                $tmp = array(
                    'name' => $resident->name,
                    'gender' => $resident->gender,
                    'homeworld' => $resident->homeworld->url,
                    'url' => $resident->url,
                );
                $this->residents[] = $tmp;
            }
        } while ($residents->hasNext());
        return $this->residents;
    }

    public function getPlanets() {
        if (!empty($this->palnets)) { //проверка строки на пустоту
            return $this->palnets;
        }
        do {
            if (!isset($palnets)) {
                $palnets = $this->swapi->planets()->index();
            } else {
                $palnets = $palnets->getNext();
            }

            foreach ($palnets as $planet) {
                $tmp = array(
                    'name' => $planet->name,
                    'rotation_period' => $planet->rotation_period,
                    'orbital_period' => $planet->orbital_period,
                    'diameter' => $planet->diameter,
                    'climate' => $planet->climate,
                    'gravity' => $planet->gravity,
                    'terrain' => $planet->terrain,
                    'surface_water' => $planet->surface_water,
                    'population' => $planet->population,
                    'url' => $planet->url,
                );
                if (!empty($planet->residents)) {
                    foreach ($planet->residents as $resident) {
                        $tmp['residents'][] = $resident->url;
                    }
                }
                $this->palnets[] = $tmp;
            }
        } while ($palnets->hasNext());
        return $this->palnets;
    }

}
